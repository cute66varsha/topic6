## Below are the files for DSC-540-O500 Topic 6

-----------------------
***Part 1 Theory***

DSC540_0500_Topic6_Part1_Theory.docx   

***Part 2 Fuzzy Model***

DSC540_0500_Topic6_Part2.ipynb         
DSC540_0500_Topic6_Part2_2.ipynb

-----------------------
**To Execute the file**

The file can be opened in Jupytor notebook and execute to view the output

-----------------------
**Result**

DSC540_0500_Topic6_Part2_Result.docx
